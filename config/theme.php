<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Title
    |--------------------------------------------------------------------------
    |
    | This option controls the default <title></title> text.
    |
    */

    'title' => 'My Site',

    /*
    |--------------------------------------------------------------------------
    | Site Name
    |--------------------------------------------------------------------------
    |
    | This option controls the site name. Name should be all caps.
    |
    */

    'name' => 'My Site',

    /*
    |--------------------------------------------------------------------------
    | Logo IMG
    |--------------------------------------------------------------------------
    |
    | This option controls the logo path and alt text.
    |
    */

    'logo' => [
        'inverted' => [
            'path' => '/images/SBLogoInverted.png'
        ],
        'path' => '/images/SBLogo.png',
        'alt' => 'SB'
    ],

    'profile' => [
        'path' => '/images/ProfilePic.jpg',
        'alt' => 'Profile Pic'
    ]

];
