@extends('layout.app')

@section('content')

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-one-third">
                    <div class="box">
                        <img src="{{ config('theme.profile.path') }}" alt="{{ config('theme.profile.alt') }}" class="Header__logo-img">
                    </div>
                </div>
                <div class="column">
                    <div>
                        <h1 class="title">Professional</h1>
                        My name is Seth Ballance, and I am a computer engineering student at Walla Walla University.
                        I am going into my junior year and plan to graduate fall of 2019. I work as a student developer
                        in the WWU IT department, mostly working on the university's web applications, keeping them up
                        to date. I have also done some work on UWP applications, but my specialty is UI design. I enjoy
                        making a site pleasurable to the eye and easy to understand and follow.
                        <hr>
                        <h1 class="title">Background</h1>
                        I grew up in the towns of Galt and Herald, located in the Sacramento valley. I lived there with
                        my parents and three siblings until I went off to Rio Lindo Adventist Academy, a boarding school.
                        Rio is where I started to get interested in computer technology. I had never done much with computers
                        before, but my first job was as a computer lab worker. I mostly just helped people if they needed
                        help with printing or with working on some Microsoft document, but I also did some computer
                        updating and managing. I also started learning to code at Rio. I took my first coding class, web
                        design, my sophomore year and enjoyed it so much that I wanted to learn more. Every once in a while
                        I would decide to make some simple site for myself, but I couldn't do much of anything very
                        complicated. At Rio, I was also very involved in the athletics department, playing in every sport
                        offered. My favorite was flag football, where I was chosen as MVP both my junior and senior years.
                        I stayed at Rio for all four years and really enjoyed it there, but by the end I was ready to
                        move on to something new. I moved on to Walla Walla University for my freshman year of college
                        because of their reputable engineering department. This is where I started to learn much more
                        about coding. I took programming classes all year where I learned to work with C++. It wasn't
                        my favorite language, but it was still helpful in learning coding basics. I also had a job in
                        computer support for the first quarter, but didn't really like it because of all the phone
                        calls I had to take. I told my friend, Nathan Isaac, who is also the head of the university's
                        web development department about looking for another job, and he suggested that I try working
                        for him. I started out feeling like I was trying to drink from a fire hose, but eventually I
                        started to understand things a lot better. I was mostly learning about Laravel, the php based
                        web framework, which is the main thing I work with now. I have enjoyed what I have learned, and
                        I'm always looking to learn more.
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection