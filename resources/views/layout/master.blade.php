<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" >
<head>
    @include('partials.head')
</head>
<body>
    <div id="app" class="content">
        @yield('master.content')
    </div>

    @include('partials.scripts')

    @yield('master.scripts')
</body>