<section class="Header">
    <nav class="nav">
        <div class="container">
            <div class="nav-left">
                <a href="{{ route('welcome') }}" class="nav-item is-brand Main-nav-item">
                    <img src="{{ config('theme.logo.inverted.path') }}" alt="{{ config('theme.logo.alt') }}" class="Header__logo-img">
                </a>
            </div>

            <div class="nav-center">
                <a href="https://www.facebook.com/saballance" class="nav-item Main-nav-item">
                <span class="icon">
                    <i class="fa fa-facebook-square"></i>
                </span>
                </a>
                <a href="https://www.instagram.com/seth12thman/?hl=en" class="nav-item Main-nav-item">
                <span class="icon">
                    <i class="fa fa-instagram"></i>
                </span>
                </a>
            </div>

            <!-- This "nav-toggle" hamburger menu is only visible on mobile -->
            <!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
            <span class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </div>
    </nav>
</section>
<section class="Subheader">
    <nav class="nav has-shadow">
        <div class="container">
            <div class="nav-left nav-menu">
                <a href="{{ route('welcome') }}" class="nav-item">
                    Home
                </a>
                <a href="{{ route('about') }}" class="nav-item">
                    About
                </a>
            </div>
        </div>
    </nav>
</section>